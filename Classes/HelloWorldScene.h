#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include "vector"

class HelloWorld : public cocos2d::Layer
{
public:
    HelloWorld();
    static cocos2d::Scene* createScene();

    virtual bool init();
    virtual void onEnter();
    virtual bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unused_event);
    virtual void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *unused_event);
//    bool onContactBegin(cocos2d::PhysicsContact& contact);

    void menuCloseCallback(cocos2d::Ref* pSender);
    void menuCloseCallback2(cocos2d::Ref* pSender);

    void updateOnce(float dt);

    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);
private:
     std::vector<cocos2d::Sprite*> _sprites;
     int _counter;
     bool _isFlying;
     cocos2d::Layer* gameLayer;
};

#endif // __HELLOWORLD_SCENE_H__

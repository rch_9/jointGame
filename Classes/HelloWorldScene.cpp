#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

HelloWorld::HelloWorld():
    _counter(0),
    _isFlying(false) {
}

Scene* HelloWorld::createScene()
{    
    auto scene = Scene::createWithPhysics();
    scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
    scene->getPhysicsWorld()->setGravity(Vec2::ZERO);    
    auto layer = HelloWorld::create();    
    scene->addChild(layer);    

    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init() {
    if ( !Layer::init() ) {
        return false;
    }
    
    auto visibleSize = Director::getInstance()->getVisibleSize();

    auto node = Node::create();
    node->setPhysicsBody(PhysicsBody::createEdgeBox(visibleSize / 1.1));
    node->setPosition(visibleSize / 2);
    this->addChild(node);

    auto background = Sprite::create("background.png");
    background->setAnchorPoint(Vec2::ZERO);
    background->setPosition(0, 0);
    gameLayer = Layer::create();
    gameLayer->setAnchorPoint(Vec2::ZERO);

    auto voidNode = ParallaxNode::create();
    voidNode->addChild(background, -1, Vec2(0.4f, 0.5f), Vec2::ZERO);
    voidNode->addChild(gameLayer, 1, Vec2(0.4f, 1.5f), Vec2::ZERO);

    auto goUp = MoveBy::create(4, Vec2(0,-500) );
    auto goDown = goUp->reverse();
    auto seq = Sequence::create(goUp, goDown, nullptr);    
    voidNode->runAction( (RepeatForever::create(seq)));

    this->addChild(voidNode);


    this->scheduleOnce(schedule_selector(HelloWorld::updateOnce), 0.2f);

    return true;
}

void HelloWorld::onEnter() {
    Layer::onEnter();

    auto listener = EventListenerTouchOneByOne::create();
    listener->onTouchBegan = CC_CALLBACK_2(HelloWorld::onTouchBegan, this);
    listener->onTouchEnded = CC_CALLBACK_2(HelloWorld::onTouchEnded, this);
//    listener->onTouchCancelled = CC_CALLBACK_2(HelloWorld::onTouchCancelled, this);

    auto dispatcher = cocos2d::Director::getInstance()->getEventDispatcher();
    dispatcher->addEventListenerWithSceneGraphPriority(listener, this);
}

//заотпускание и замах
bool HelloWorld::onTouchBegan(Touch *touch, Event *unused_event) {
    if (_isFlying) {
        auto sp1 = static_cast<Sprite*>(_sprites.at(_counter));
        auto sp2 = this->getChildByTag(10)->getChildByTag(1);
        if (sp1->getBoundingBox().intersectsCircle(sp2->getPosition(), sp2->getContentSize().width / 2.f)) {
            auto world = static_cast<Scene*>(_parent)->getPhysicsWorld();
            auto joint = PhysicsJointLimit::construct(sp1->getPhysicsBody(), sp2->getPhysicsBody(), Point::ZERO, Point::ZERO, 30.f, 60.f);
            world->addJoint(joint);

            _isFlying = false;
        }
    } else if (!_isFlying) {
        auto world = static_cast<Scene*>(_parent)->getPhysicsWorld();
        world->removeAllJoints();
        ++_counter == 3 ? _counter = 0 : _counter ;
        _isFlying = true;
    }

    return true;
}

//отвечает за бросок
void HelloWorld::onTouchEnded(Touch *touch, Event *unused_event) {
//    if (!_isFlying) {
//        auto world = static_cast<Scene*>(_parent)->getPhysicsWorld();
//        world->removeAllJoints();
//        ++_counter == 3 ? _counter = 0 : _counter ;
//        _isFlying = true;
//    }
}

void HelloWorld::menuCloseCallback(Ref *pSender) {
    auto world = static_cast<Scene*>(_parent)->getPhysicsWorld();
//    auto joint = PhysicsJointLimit::construct(sp1body, sp2body, Vec2::ZERO, Vec2::ZERO, 30.f, 60.f);

    world->removeAllJoints();
}

void HelloWorld::menuCloseCallback2(Ref *pSender) {
    auto visibleSize = Director::getInstance()->getVisibleSize();

    auto sp1 = _sprites.back();
    auto sp2 = static_cast<Sprite*>(this->getChildByTag(1));

    auto world = static_cast<Scene*>(_parent)->getPhysicsWorld();
    auto joint = PhysicsJointLimit::construct(sp1->getPhysicsBody(), sp2->getPhysicsBody(), Point::ZERO, Point::ZERO, 30.f, 60.f);

    world->addJoint(joint);
}

void HelloWorld::updateOnce(float dt) {
    auto visibleSize = Director::getInstance()->getVisibleSize();

//    auto gameLayer = this->getChildByTag(10);

    for(int i = 0; i < 3; ++i) {
        auto sp1 = Sprite::create("CloseNormal.png");
        sp1->setScale(3);
        _sprites.push_back(sp1);
        auto sp1body = PhysicsBody::createCircle(sp1->getContentSize().width / 2.f, PhysicsMaterial(0, 0, 0));
        sp1body->setDynamic(false);
        sp1body->setCategoryBitmask(0x02);    // 0010
        sp1body->setCollisionBitmask(0x02);   // 0001

        sp1->setPhysicsBody(sp1body);
        sp1->setPosition(visibleSize.width / 2, sp1->getContentSize().height * i + 200 * (i + 1));
        gameLayer->addChild(sp1);
    }

    auto sp2 = Sprite::create("CloseSelected.png");
    auto sp2body = PhysicsBody::createCircle(sp2->getContentSize().width / 2.f);
    sp2body->setVelocity(Vec2(0, 200));

    sp2body->setCategoryBitmask(0x01);    // 0010
    sp2body->setCollisionBitmask(0x02);   // 0001

    sp2->setPhysicsBody(sp2body);    
    sp2->setPosition(_sprites.at(0)->getBoundingBox().getMaxX(), _sprites.at(0)->getBoundingBox().getMidY());
    sp2->setTag(1);
    this->addChild(sp2);

    auto world = static_cast<Scene*>(_parent)->getPhysicsWorld();
    auto joint = PhysicsJointLimit::construct(_sprites.at(0)->getPhysicsBody(), sp2body, Vec2::ZERO, Vec2::ZERO, 30.f, 60.f);

    world->addJoint(joint);    
}
